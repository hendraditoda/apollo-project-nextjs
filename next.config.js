/** @type {import('next').NextConfig} */
const nextConfig = {
  async headers() {
    return [
      {
        // Mengizinkan CORS dari semua origin
        source: '/(.*)',
        headers: [
          {
            key: 'Access-Control-Allow-Origin',
            value: '*',
          },
        ],
      },
    ];
  },
};

module.exports = nextConfig;
