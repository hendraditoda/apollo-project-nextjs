import React from 'react';
import { cookies } from 'next/headers';
import axios from 'axios';
import GetAllVideos from '@/components/mediahandling/show/getVideo.components';

const getData = async () => {
  try {
    const cookieStore = cookies();
    const userId = cookieStore.get('userId').value;
    const dataVideos = await axios.get(`https://api-apollo.niceblue.my.id/api/videoMedia/${userId}`);
    const data = dataVideos.data.data.videos;
    return data;
  } catch (err) {
    console.error('Error fetching videos:', err);
  }
};

const Videos = async () => {
  const response = await getData();
  return <GetAllVideos data={response} />;
};

export default Videos;
