import React from 'react';
import FormUpdateVideo from '@/components/mediahandling/form/update/formUpdateVideo.components';

export default function EditVideoPage({ params }) {
  const { id } = params;

  return (
    <div className='bg-[#0c0d14] min-h-screen flex items-center justify-center'>
      <div className='flex flex-col items-center justify-center rounded-xl  bg-slate-400'>
        <h1 className='text-black text-2xl font-semibold py-3 mb-3'>Update Video id of :{id}</h1>
        <FormUpdateVideo videoId={id} />
      </div>
    </div>
  );
}
