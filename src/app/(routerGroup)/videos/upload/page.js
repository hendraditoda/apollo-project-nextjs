import React from 'react';
import FormVideo from '@/components/mediahandling/form/create/formVideo.components';

const UploadVideoPage = () => {
  return (
    <div className='bg-[#0c0d14] min-h-screen flex items-center justify-center'>
      <div className='flex flex-col items-center justify-center rounded-xl  bg-slate-400'>
        <h1 className='text-black text-2xl font-semibold py-3 mb-3'>Upload a Video</h1>
        <FormVideo />
      </div>
    </div>
  );
}

export default UploadVideoPage;