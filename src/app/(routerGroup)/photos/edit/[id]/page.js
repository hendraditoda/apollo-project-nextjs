import React from 'react';
import FormUpdatePhoto from '@/components/mediahandling/form/update/formUpdatePhoto.components';

export default function EditPhotoPage({ params }) {
  const { id } = params;

  return (
    <div className='bg-[#0c0d14] min-h-screen flex items-center justify-center'>
      <div className='flex flex-col items-center justify-center rounded-xl  bg-slate-400'>
        <h1 className='text-black text-2xl font-semibold py-3 mb-3'>Update Photo id of :{id}</h1>
        <FormUpdatePhoto photoId={id} />
      </div>
    </div>
  );
}
