import React from 'react';
import GetAllPhotos from '@/components/mediahandling/show/getPhoto.components';
import { cookies } from 'next/headers';
import axios from 'axios';

const getData = async () => {
  try {
    const cookieStore = cookies();
    const userId = cookieStore.get('userId').value;
    // const dataPhotos = await axios.get(`https://api-apollo.niceblue.my.id/api/photoMedia/${userId}`);
    const dataPhotos = await axios.get(`https://api-apollo.niceblue.my.id/api/photoMedia/${userId}`);
    const data = dataPhotos.data.data.photos;
    return data;
  } catch (err) {
    console.error('Error fetching photos:', err);
  }
};

const Photos = async () => {
  const response = await getData();
  return <GetAllPhotos data={response} />;
};

export default Photos;
