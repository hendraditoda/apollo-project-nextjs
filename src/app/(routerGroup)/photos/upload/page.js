import React from 'react';
import FormPhoto from '@/components/mediahandling/form/create/formPhoto.components';

const UploadPhotoPage = () => {
  return (
    <div className='bg-[#0c0d14] min-h-screen flex items-center justify-center'>
      <div className='flex flex-col items-center justify-center rounded-xl  bg-slate-400'>
        <div className='flex flex-row'>
          <h1 className='text-black text-2xl font-semibold py-3 mb-3'>Upload a Photo</h1>
        </div>
        <FormPhoto />
      </div>
    </div>
  );
}

export default UploadPhotoPage;