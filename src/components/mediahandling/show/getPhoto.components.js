'use client';

import React, { useEffect, useState } from 'react';
import Image from 'next/legacy/image';
import Link from 'next/link';
import axios from 'axios';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import Spinner from '@/components/Spinner/Spinner';

const GetAllPhotos = ({ data }) => {
  const [photos, setPhotos] = useState([]);
  const [selectedPhotoId, setSelectedPhotoId] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const handleMouseEnter = (photoId) => {
    setSelectedPhotoId(photoId);
  };

  const handleMouseLeave = () => {
    setSelectedPhotoId(null);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setPhotos(data);
        setIsLoading(true);
      } catch (err) {
        console.error('Error fetching photos:', err);
      }
    };

    fetchData();
  }, [data]);

  const handleDelete = async (photoId) => {
    try {
      await axios.delete(`https://api-apollo.niceblue.my.id/api/photoMedia/delete/${photoId}`);
      setPhotos((prevPhotos) => prevPhotos.filter((photo) => photo.id !== photoId));
    } catch (err) {
      console.error('Error deleting photo:', err);
    }
  };

  return (
    <div className="min-h-screen">
      <div className="overflow-x">
        <table className="table-auto overflow-scroll w-full text-sm text-left text-gray-500 dark:text-gray-400 py-5">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="pl-1">
                ID
              </th>
              <th scope="col" className="px-6 py-3">
                Photos
              </th>
              <th scope="col" className="px-4 py-3">
                Description
              </th>
              <th scope="col" className="px-4 py-3">
                Created at
              </th>
              <th scope="col" className="px-4 py-3">
                Updated at
              </th>
              <th scope="col" className="py-3">
                <div className="flex items-center">
                  <span className="mr-2">Action</span>
                  <Link href="/photos/upload">
                    <AddCircleIcon sx={{ color: '#fb6f92' }} fontSize="medium" />
                  </Link>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            {isLoading ? Spinner : <p>Sedang mengambil data ...</p>}
            {photos?.length > 0 ? (
              photos.map((photo) => {
                const createdAt = new Date(photo.createdAt).toLocaleString();
                const updatedAt = new Date(photo.updatedAt).toLocaleString();
                const isHovered = photo.id === selectedPhotoId;
                return (
                  <tr key={photo.id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <td className="pl-1 font-medium text-gray-900 whitespace-nowrap dark:text-white">{photo.id}</td>
                    <td className="px-6 py-4">
                      <a
                        href={`https://api-apollo.niceblue.my.id/photo/${photo.name}`}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <Image
                          loader={() => `https://api-apollo.niceblue.my.id/photo/${photo.name}`}
                          src={`https://api-apollo.niceblue.my.id/photo/${photo.name}`}
                          blurDataURL={`https://api-apollo.niceblue.my.id/photo/${photo.name}`}
                          alt={photo.originalName}
                          unoptimized={true}
                          layout="responsive"
                          width={100}
                          height={100}
                          placeholder="blur"
                        />
                      </a>
                    </td>
                    <td className="px-4 py-3">{photo.description}</td>
                    <td className="px-4 py-3">{createdAt}</td>
                    <td className="px-4 py-3">{updatedAt}</td>
                    <td className="py-3">
                      <Link href={`/photos/edit/${photo.id}`}>
                        <EditIcon sx={{ color: '#2563eb', marginRight: '10px' }} fontSize="medium" />
                      </Link>
                      <DeleteIcon
                        sx={{ color: isHovered ? '#db2424' : 'grey' }}
                        fontSize="medium"
                        onMouseEnter={() => handleMouseEnter(photo.id)}
                        onMouseLeave={handleMouseLeave}
                        onClick={() => handleDelete(photo.id)}
                      />
                    </td>
                  </tr>
                );
              })
            ) : (
              <tr>
                <td colSpan="4">No photos found.</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default GetAllPhotos;
