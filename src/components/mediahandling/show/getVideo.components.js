'use client';

import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import ReactPlayer from 'react-player';
import axios from 'axios';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import Spinner from '@/components/Spinner/Spinner';

const GetAllVideos = ({ data }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [videos, setVideos] = useState([]);
  const [selectedVideoId, setSelectedVideoId] = useState(null);

  const handleMouseEnter = (videoId) => {
    setSelectedVideoId(videoId);
  };

  const handleMouseLeave = () => {
    setSelectedVideoId(false);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setVideos(data);
        setIsLoading(true);
      } catch (err) {
        console.error('Error fetching videos:', err);
      }
    };
    fetchData();
  }, [data]);

  const handleDelete = async (videoId) => {
    try {
      await axios.delete(`https://api-apollo.niceblue.my.id/api/videoMedia/delete/${videoId}`);
      setVideos((prevVideos) => prevVideos.filter((video) => video.id !== videoId));
    } catch (err) {
      console.error('Error deleting video:', err);
    }
  };

  return (
    <div className="min-h-screen">
      <div className="overflow-x">
        <table className="table-auto overflow-scroll w-full text-sm text-left text-gray-500 dark:text-gray-400 py-5">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="pl-1">
                ID
              </th>
              <th scope="col" className="px-6 py-3">
                Videos
              </th>
              <th scope="col" className="px-4 py-3">
                Description
              </th>
              <th scope="col" className="px-4 py-3">
                Created at
              </th>
              <th scope="col" className="px-4 py-3">
                Updated at
              </th>
              <th scope="col" className="py-3">
                <div className="flex items-center">
                  <span className="mr-2">Action</span>
                  <Link href="/videos/upload">
                    <AddCircleIcon sx={{ color: '#fb6f92' }} fontSize="medium" />
                  </Link>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            {isLoading ? Spinner : <p>Sedang mengambil data ...</p>}
            {videos?.length > 0 ? (
              videos.map((video) => {
                const createdAt = new Date(video.createdAt).toLocaleString();
                const updatedAt = new Date(video.updatedAt).toLocaleString();
                const isHovered = video.id === selectedVideoId;
                return (
                  <tr key={video.id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <td className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                      {video.id}
                    </td>
                    <td className="px-6 py-4">
                      <ReactPlayer
                        width={350}
                        height={200}
                        url={`https://api-apollo.niceblue.my.id/video/${video.name}`}
                        controls={true}
                      />
                    </td>
                    <td className="px-6 py-4">{video.description}</td>
                    <td className="px-4 py-3">{createdAt}</td>
                    <td className="px-4 py-3">{updatedAt}</td>
                    <td className="px-6 py-4">
                      <Link href={`/videos/edit/${video.id}`}>
                        <EditIcon sx={{ color: '#2563eb' }} fontSize="medium" />
                      </Link>
                      <DeleteIcon
                        sx={{ color: isHovered ? '#db2424' : 'grey' }}
                        fontSize="medium"
                        onMouseEnter={() => handleMouseEnter(video.id)}
                        onMouseLeave={handleMouseLeave}
                        onClick={() => handleDelete(video.id)}
                      />
                    </td>
                  </tr>
                );
              })
            ) : (
              <tr>
                <td colSpan="4">No Videos found.</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default GetAllVideos;
