'use client';

import { useRouter } from 'next/navigation';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import Button from '../../button/buttonMedia.components';

const FormUpdatePhoto = ({ photoId }) => {
  const router = useRouter();
  const [description, setDescription] = useState('');
  const [file, setFile] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const userId = Cookies.get('userId');
        const response = await axios.get(`https://api-apollo.niceblue.my.id/api/photoMedia/${userId}`);
        const { data } = response;
        if (data && data.data.photos.length) {
          const selectedPhoto = data.data.photos.find((photo) => photo.id === Number(photoId));
          if (selectedPhoto) {
            const description = selectedPhoto.description;
            setDescription(description || '');
          } else {
            console.log(`Photo with ID ${photoId} not found.`);
          }
        }
      } catch (err) {
        console.error('Error fetching photos:', err);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
  }, [photoId]);

  const handleDescriptionChange = (e) => {
    setDescription(e.target.value);
  };

  const handleFileChange = (e) => {
    setFile(e.target.files[0]);
  };

  const handleUpdateSubmit = async (e) => {
    e.preventDefault();

    if (!description || !file) {
      setErrorMessage('Please fill in all fields.');
      return;
    }

    setIsLoading(true);
    setErrorMessage('');

    const formData = new FormData();
    formData.append('description', description);
    formData.append('photo', file);

    try {
      const response = await axios.put(`https://api-apollo.niceblue.my.id/api/photoMedia/update/${photoId}`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      router.push('/photos');
      router.refresh('/photos');
      console.log('Photo updated:', response.data);
    } catch (error) {
      console.error('Failed to update photo:', error);
      setErrorMessage('Failed to update photo. Please try again.');
    } finally {
      setIsLoading(false);
    }
  };

  const handleBack = () => {
    router.back();
  };

  return (
    <div className="px-10">
      <form className="text-black mb-2 flex flex-col" onSubmit={handleUpdateSubmit}>
        {errorMessage && <p>{errorMessage}</p>}
        <label htmlFor="description" className="text-black mb-2">
          Description:
        </label>
        <input
          type="text"
          id="description"
          value={description}
          onChange={handleDescriptionChange}
          className="text-black rounded-xl px-4 py-2 mb-4"
        />
        <div className="py-5 flex flex-row">
          <label htmlFor="file" className="py-2">
            Please select a file:
          </label>
          <input
            type="file"
            id="file"
            accept="image/*"
            onChange={handleFileChange}
            className="text-black rounded-xl px-2 py-2 mb-4"
          />
          <Button onClick={handleUpdateSubmit} label="Update" isLoading={isLoading}>
            {isLoading ? 'Updating...' : 'Update Photo'}
          </Button>
          <Button onClick={handleBack} label="Back" />
        </div>
      </form>
    </div>
  );
};

export default FormUpdatePhoto;
