'use client';

import { useRouter } from 'next/navigation';
import React, { useState } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import Button from '../../button/buttonMedia.components';

export default function FormVideo() {
  const router = useRouter();
  const userId = Cookies.get('userId');
  const [isLoading, setIsLoading] = useState(false);
  const [selectedFile, setSelectedFile] = useState(null);
  const [description, setDescription] = useState('');

  // for file
  const handleFileChange = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  // for description
  const handleDescriptionChange = (e) => {
    setDescription(e.target.value);
  };

  const handleVideoSubmit = async (e) => {
    e.preventDefault();

    if (selectedFile) {
      //API calls here
      try {
        setIsLoading(true);
        const formData = new FormData();
        formData.append('video', selectedFile);
        formData.append('description', description);

        const response = await axios.post(`https://api-apollo.niceblue.my.id/api/videoMedia/${userId}`, formData, {
          headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'multipart/form-data',
          },
        });

        router.push('/videos');
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    }
  };

  const handleBack = () => {
    router.back();
  };

  return (
    <div className="px-10">
      <form onSubmit={handleVideoSubmit} className="text-black mb-2 flex flex-col">
        <label htmlFor="description" className="text-black mb-2">
          Description:
        </label>
        <input
          type="text"
          id="description"
          onChange={handleDescriptionChange}
          className="text-black rounded-xl px-4 py-2 mb-4"
        />
        <div className="py-5 flex flex-row">
          <label htmlFor="file" className="py-2">
            Please select a video to upload:
          </label>
          <input
            type="file"
            id="file"
            onChange={handleFileChange}
            accept="video/mp4,video/x-m4v,video/*"
            className="text-black rounded-xl px-2 py-2 mb-4"
          />
          {selectedFile && <Button onClick={handleVideoSubmit} label="Upload" isLoading={isLoading} />}
          <Button onClick={handleBack} label="Cancel" />
        </div>
      </form>
    </div>
  );
}
