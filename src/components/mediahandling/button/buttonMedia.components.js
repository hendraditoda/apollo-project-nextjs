import React from 'react';
import Spinner from '@/components/Spinner/Spinner';


const Button = ({ onClick, label, isLoading }) => {
  let buttonColor = '';

  if (label === 'Update' || label === 'Upload') {
    buttonColor = 'bg-[#00a3e1] hover:bg-blue-800';
  } else if (label === 'Back' || label === 'Cancel') {
    buttonColor = 'bg-[#fb6f92] hover:bg-pink-800';
  }

  return (
    <button
      type="button"
      onClick={onClick}
      disabled={isLoading}
      className={` text-white font-bold py-2 px-4 mx-3 rounded-xl ${buttonColor} ${isLoading ? 'opacity-50 cursor-not-allowed' : ''}`}
    >
      {isLoading ? <Spinner /> : label}
    </button>
  );
};

export default Button;