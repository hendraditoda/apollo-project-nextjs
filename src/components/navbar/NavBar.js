'use client';

import Image from 'next/image';
import Link from 'next/link';
import Cookies from 'js-cookie';
import { useRouter } from 'next/navigation';
import { useState, useEffect } from 'react';
import Spinner from '../Spinner/Spinner';
import '../../app/globals.css';

export default function Navbar() {
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  const handleSignUp = (event) => {
    router.push('/authentication/register');
    setIsLoading(true);
  };
  const handleLogOut = (event) => {
    Cookies.remove('token');
    Cookies.remove('userId');
    Cookies.remove('username');
    router.push('/');
    setIsLoading(true);
  };

  const isLoggedIn = Cookies.get('token');

  return (
    <nav className="bg-[#1f2030]">
      <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
        <div className="relative flex items-center justify-between h-16">
          <div className="flex-shrink-0 flex-items-center">
            <Link
              href="/home"
              className="flex items-center text-white no-underline font-bold text-xl"
            >
              <Image src="/apollo.png" alt="Logo" width={24} height={24} />
              <span className="ml-2">Apollo Games</span>
            </Link>
          </div>
          <div className="hidden md:block">
            <div className="ml-4 flex items-center">
              <Link href="/gamedetail" className="mr-4 no-underline text-white">
                Leaderboard
              </Link>
              <Link href="/profile" className="mr-4 no-underline text-white">
                Profile
              </Link>
              <Link href="/photos" className="mr-4 no-underline text-white">
                Photos
              </Link>
              <Link href="/videos" className="mr-4 no-underline text-white">
                Videos
              </Link>
              {isLoggedIn ? (
                <>
                  <Link
                    href="/gamecatalog"
                    className="ml-4 no-underline text-white"
                  >
                    myGames
                  </Link>
                  <Image
                    src="/person.png"
                    alt="Profile Picture"
                    width={32}
                    height={32}
                    className="ml-6 rounded-full inline-block border border-white overflow-hidden"
                  />
                  <button
                    onClick={handleLogOut}
                    className="bg-[#5c7cfa] rounded-full text-white ml-4 w-20"
                  >
                    {isLoading ? <Spinner /> : 'Log out'}
                  </button>
                </>
              ) : (
                <button
                  onClick={handleSignUp}
                  className="bg-[#5c7cfa] rounded-full text-white ml-4 w-20"
                >
                  {isLoading ? <Spinner /> : 'Sign Up'}
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}
