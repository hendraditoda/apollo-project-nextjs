describe('Page Gameplay', () => {
  it('should visit the gameplay with userid as params', () => {
    const userId = 1;
    cy.setCookie('userId', '1');
    cy.visit(`/gameplay/${userId}`);
    cy.get('h1').should('contain', 'Back to home');
    cy.get('h1').should('contain', 'SCORE POINT');
    cy.get('h1').should('contain', 'VS');
    cy.get('h2').should('contain', 'PLAYER');
    cy.get('h2').should('contain', 'COMPUTER');
    // cy.get("img").click().should("have.class", "active");
  });
});
