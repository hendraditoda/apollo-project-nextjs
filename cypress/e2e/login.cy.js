// describe('Form Login', () => {
//   beforeEach(() => {
//     cy.visit('/authentication/login');
//   });

//   it('should login successfully', () => {
//     cy.server();
//     cy.route({
//       method: 'POST',
//       url: 'https://api-apollo.niceblue.my.id/api/authentication/login',
//       response: {
//         data: {
//           token:
//             'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJrYWtla3pldXMiLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjg4Mjk2NDcxLCJleHAiOjE2ODgzODI4NzF9.Ti71ypQBPE2TgUhMkJUoDGITTiu4dgovTbl1q5QJwpk',
//         },
//         message: 'Login successful',
//       },
//     });

//     cy.get('#username').type('your_username');
//     cy.get('#password').type('your_password');
//     cy.get('form').submit();

//     cy.url().should('include', '/home');
//     cy.getCookie('token').should('have.property', 'value', 'example_token');
//   });

//   it('should display error message for invalid login', () => {
//     cy.server();
//     cy.route({
//       method: 'POST',
//       url: 'https://api-apollo.niceblue.my.id/api/authentication/login',
//       status: 400,
//       response: {
//         error: 'Invalid username or password',
//       },
//     });

//     cy.get('#username').type('invalid_username');
//     cy.get('#password').type('invalid_password');
//     cy.get('form').submit();

//     cy.get('.error-message').should('contain', 'Invalid username or password');
//   });
// });

// describe('Form Login', () => {
//   beforeEach(() => {
//     cy.visit('/');
//   });

//   it('should login successfully with valid credentials', () => {
//     cy.server();
//     cy.route('POST', 'https://api-apollo.niceblue.my.id/api/authentication/login').as('loginRequest');

//     cy.get('#username').type('your-username');
//     cy.get('#password').type('your-password');
//     cy.contains('Sign In').click();

//     cy.wait('@loginRequest').then((xhr) => {
//       expect(xhr.status).to.equal(200);
//       expect(xhr.response.body).to.have.property('data');
//       expect(xhr.response.body.data).to.have.property('token');
//       // Add more assertions if needed
//     });

//     cy.url().should('include', '/home');
//   });

//   it('should display error message with invalid credentials', () => {
//     cy.server();
//     cy.route({
//       method: 'POST',
//       url: 'https://api-apollo.niceblue.my.id/api/authentication/login',
//       status: 400,
//       response: { error: 'Invalid credentials' },
//     }).as('loginRequest');

//     cy.get('#username').type('invalid-username');
//     cy.get('#password').type('invalid-password');
//     cy.contains('Sign In').click();

//     cy.wait('@loginRequest').then((xhr) => {
//       expect(xhr.status).to.equal(400);
//       expect(xhr.response.body).to.have.property('error', 'Invalid credentials');
//       // Add more assertions if needed
//     });

//     cy.contains('Berhasil Login').should('not.exist');
//     cy.contains('Invalid credentials').should('be.visible');
//     cy.url().should('not.include', '/home');
//   });
// });

// describe('Form Login', () => {
//   it('should be able to login successfully', () => {
//     cy.visit('/authentication/login'); // Ganti dengan URL halaman login Anda jika perlu

//     // Masukkan username dan password
//     cy.get('#username').type('kakekzeus');
//     cy.get('#password').type('123456');

//     // Submit form
//     cy.get('form').submit();

//     // Verifikasi bahwa pengalihan halaman terjadi setelah login berhasil
//     cy.location('pathname').should('eq', '/home');
//   });

//   it('should display an error message for invalid login', () => {
//     cy.visit('/authentication/login'); // Ganti dengan URL halaman login Anda jika perlu

//     // Masukkan username dan password yang salah
//     cy.get('#username').type('kakekzeus');
//     cy.get('#password').type('123123');

//     // Submit form
//     cy.get('form').submit();

//     // Verifikasi bahwa pesan error ditampilkan
//     cy.contains('Login failed, username/password does not match').should('be.visible');
//   });

//   it(`should redirect to registration page when "Don't have account?" link is clicked`, () => {
//     cy.visit('/authentication/register'); // Ganti dengan URL halaman login Anda jika perlu

//     // Klik link "Don't have account?"
//     cy.contains('Dont have account ? Please Register').click();

//     // Verifikasi bahwa pengalihan halaman terjadi ke halaman registrasi
//     cy.location('pathname').should('eq', '/authentication/register');
//   });
// });

// describe('The Login Page', () => {
//   beforeEach(() => {
//     // reset and seed the database prior to every test
//     cy.exec('npm run db:reset && npm run db:seed');

//     // seed a user in the DB that we can control from our tests
//     // assuming it generates a random password for us
//     cy.request('POST', '/authentication/login', { username: 'kakekzeus' }, { failOnNonZeroExit: false })
//       .its('body')
//       .as('currentUser');
//   });

//   it('sets auth cookie when logging in via form submission', function () {
//     // destructuring assignment of the this.currentUser object
//     const { username, password } = this.currentUser;

//     cy.visit('/login');

//     cy.get('input[name=username]').type(username);

//     // {enter} causes the form to submit
//     cy.get('input[name=password]').type(`${password}{enter}`);

//     // we should be redirected to /dashboard
//     cy.url().should('include', '/dashboard');

//     // our auth cookie should be present
//     cy.getCookie('your-session-cookie').should('exist');

//     // UI should reflect this user being logged in
//     cy.get('h1').should('contain', 'jane.lane');
//   });
// });
describe('FormLogin', () => {
  it('should submit the login form successfully', () => {
    cy.visit('/authentication/login'); // Replace with the URL of your Next.js application

    cy.get('#username').type('kakekzeus');
    cy.get('#password').type('123456');

    cy.get('form').submit();

    cy.url().should('include', '/home');
  });

  it('should display an error message for incorrect login credentials', () => {
    cy.visit('/authentication/login'); // Replace with the URL of your Next.js application

    cy.get('#username').type('kakekzeus');
    cy.get('#password').type('123213');

    cy.get('form').submit();

    cy.get('.alert').should('contain', 'Login failed, username/password does not match');
  });
});
